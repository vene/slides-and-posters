# todo

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

# plt.style.use('seaborn-poster')
mpl.rcParams['font.size'] = 25
fs_small = (3.5, 6)
fs_big = (9, 7)

# multinli
plt.figure(figsize=fs_small, constrained_layout=True)
y = [76.05, 75.54, 76.13]

plt.bar(np.arange(len(y)), y)
plt.axhline(y[0], ls=":", color='k')
plt.ylim(72, 77)
plt.xticks(())
plt.savefig("multinli.pdf")

# snli
plt.figure(figsize=fs_small, constrained_layout=True)
y = [86.52, 86.62, 86.05]

plt.bar(np.arange(len(y)), y)
plt.axhline(y[0], ls=":", color='k')
plt.ylim(85, 87)
plt.xticks(())
plt.yticks((85, 85.5, 86, 86.5, 87))
plt.savefig("snli.pdf")

exit()

# parsing

ssvm = [
    87.02,  # en
    81.94,  # zh
    69.42,  # vi
    87.58,  # ro
    96.24   # ja
]

crf = [
    86.74,  # en
    83.18,  # zh
    69.10,  # vi
    87.13,  # ro
    96.09   # ja
]

smap = [
    86.90,  # en
    84.03,  # zh
    69.71,  # vi
    87.35,  # ro
    96.04   # ja
]

msmap = [
    87.34,  # en
    82.63,  # zh
    70.87,  # vi
    87.63,  # ro
    96.03   # ja
]

width = 0.2  # the width of the bars

ind = np.arange(len(ssvm))
fig, ax = plt.subplots(figsize=fs_big, constrained_layout=True)
ax.bar(ind - ((3/2) * width), ssvm, width,
       color='C0', label='ssvm')
ax.bar(ind - width/2, crf, width,
       color='C1', label='crf')
ax.bar(ind + width/2, smap, width,
       color='C2', label='smap')
ax.bar(ind + ((3/2) * width), msmap, width,
       color='C3', label='m-smap')

plt.ylim((60, None))

# Add some text for labels, title and custom x-axis tick labels, etc.
# ax.set_ylabel('Scores')
ax.set_xticks(())
# ax.set_xticks(ind)
# ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))
ax.legend(fancybox=False, labelspacing=.03)
plt.savefig("ud.svg")
