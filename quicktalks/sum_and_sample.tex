
\documentclass[xcolor=table,dvipsnames]{beamer}

\usepackage{mathtools}
\usepackage{amsmath,amssymb,bm}
\usepackage{tikz-dependency}
\usepackage{tikz}
\usepackage{tikz-qtree}
\usepackage{etex}
\usepackage{pgf}
\usepackage{framed}
\usepackage[utf8]{inputenc}
%\usepackage[default]{lato}
\usepackage{booktabs}
\usepackage[default]{sourcesanspro}
\usepackage[T1]{fontenc}
\usepackage{xcolor,colortbl}
\usepackage[normalem]{ulem}
\usepackage{nicefrac}

\usepackage{fancybox}
\usepackage{bchart}
\usepackage{ifthen}

\definecolor{mplblue}{HTML}{1F77B4}
\definecolor{mplorange}{HTML}{FF7F03}

\setbeamertemplate{enumerate items}[default]
%\definecolor{tPeony}{RGB}{253,110,138}
\definecolor{tPeony}{RGB}{194,76,99}
\definecolor{tBlue}{RGB}{40,126,145}
\colorlet{myhl}{tBlue!25!white}
\colorlet{mygr}{black!40}
\newcommand{\palert}[1]{\textcolor{tPeony}{#1}}
\beamertemplatenavigationsymbolsempty
\setbeamercolor{alerted text}{fg=tBlue}
%\setbeamercolor{frametitle}{bg=tBlue}
\setbeamercolor{palette primary}{bg=tBlue}
\setbeamercolor{palette secondary}{bg=tBlue!50!black}
\setbeamercolor{palette tertiary}{bg=tBlue!75!black}
\setbeamercolor{palette quaternary}{bg=tBlue}
\setbeamercolor{structure}{fg=tBlue} % itemize, enumerate, etc

\newcommand\EE{\mathbb{E}}
\newcommand\RR{\mathbb{R}}
\newcommand\ZZ{\mathcal{Z}}
\newcommand\defeq{\coloneqq}
\def\newblock{}

\title{Estimating Stochastic Gradients \\ for Discrete Latent Variables}
\author{Vlad Niculae}

\begin{document}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
Based on the following papers.

\begin{itemize}
\item
\textbf{Rao-Blackwellized Stochastic Gradients for Discrete Distributions.}
Runjing Liu, Jeffrey Regier, Nilesh Tripuraneni, Michael I. Jordan, Jon
McAuliffe.
ICML 2019.
\item
\textbf{Estimating Gradients for Discrete Random Variables by Sampling without
Replacement.}
Wouter Kool, Herke van Hoof, Max Welling.
ICLR 2020.
\end{itemize}

\emph{Thanks to Wouter Kool for discussion!}

\end{frame}

\begin{frame}{Monte Carlo Estimation}
Random variable $Z$ with values in a set $\ZZ$.
\begin{equation}
\EE[f(Z)] = \sum_{z \in \ZZ} p(z)\,f(z)
\end{equation}
\uncover<2->{
MC estimator of expectation:
\begin{enumerate}
\item Sample $z \sim p(z)$
\item $\EE[f(Z)] \approx f(z)$
\end{enumerate}}

\uncover<3->{
Now, say we have some parameters we want to optimize.


\begin{equation*}
\begin{aligned}
\nabla_\theta \EE[f_\theta(Z)]
&= \nabla_\theta \sum_{z \in \ZZ} p_\theta(z)\,f_\theta(z) \\
\uncover<4->{&= \sum_z \nabla_\theta(p(z)\,f_\theta(z)) \\}
\uncover<5->{&=
\underbrace{\sum_z p(z) \nabla_\theta f_\theta(z)}_{\EE[\nabla f(Z)]} +
\underbrace{\sum_z \nabla_\theta p_\theta(z) f_\theta(z)}_{???}}
\end{aligned}
\end{equation*}
}
\end{frame}

\begin{frame}{Score Function Estimator}

\begin{equation}
\nabla_\theta \EE[f_\theta(Z)]
=
\underbrace{\sum_z p(z) \nabla_\theta f_\theta(z)}_{\EE[\nabla f(Z)]} +
\underbrace{\sum_z \nabla_\theta p_\theta(z) f_\theta(z)}_{???}
\end{equation}

\uncover<2->{
We use $\nabla \log p(z) = \frac{\nabla p(z)}{p(z)}$:

\begin{equation}
\begin{aligned}
\sum_z f(z) \nabla_\theta p_\theta(z) &=
\sum_z p(z) f(z) \nabla \log p(z) \\
&= \EE[f(z) \nabla \log p(z) ]
\end{aligned}
\end{equation}}

\uncover<3->{
Combining yields the SFE $\nabla \EE[f_\theta(Z)] = \EE[g(Z)]$, where
\begin{equation}
g(z) = \nabla f_\theta(z) + f(z) \nabla \log p(z).
\end{equation}}

\uncover<4->{%
\alert{Terminology alert:} the score function
$s(\theta) \coloneqq \nabla_\theta \log p_\theta(z)$
}
\end{frame}


\begin{frame}{Expectations are counterintuitive!}
\vspace{-2\baselineskip}
$$\theta \in \RR \qquad Z \sim \text{Bernoulli}(\sigma(\theta))
\qquad
t = .60$$
\uncover<2->{%
$$ \min_\theta \EE (Z - t)^2 \quad \text{versus} \quad (\EE[Z] - t)^2 $$
}%
\uncover<3->{
\begin{equation*}
\begin{aligned}
\color{mplblue}(\EE[Z] - t)^2  &= (\sigma(\theta) - t)^2
~~\color{gray}(\text{because } \EE[Z] = \sigma(\theta))
\\\uncover<4->{%
\color{mplorange}\EE[(Z - t)^2]  &=
\sigma(\theta) (1 - t)^2 + \big(1 - \sigma(\theta)\big)(0 - t)^2 \\
&= \sigma(\theta)(1-2t) + t^2}
\end{aligned}
\end{equation*}
}%
\centering\uncover<5->{\includegraphics[width=\textwidth]{toy.pdf}}
\end{frame}

\begin{frame}
If $|\ZZ|$ is small enough,

\begin{equation}
    \nabla \EE[f(Z)] = \EE[g(Z)] = \sum_z p(z) g(z), \qquad\text{where}
\end{equation}
\begin{equation}
g(z) = \nabla f(z) + f(z) \nabla \log p(z).
\end{equation}

can be computed \textbf{explicitly}.

\bigskip

If $|\ZZ|$ is too large, we need ways to (Monte Carlo) approximate it.
\begin{equation}
\nabla \EE[f(Z)] = \EE[g(Z)]
\end{equation}

\begin{itemize}
\item single sample: $z \sim p$, yield $g(z)$
\item k samples (with replacement): $z_1, \dots, z_k \sim p$, yield $\frac{1}{k}
\sum_i g(z_i)$
\item better ways?
\end{itemize}
\end{frame}

\begin{frame}{Sum-and-sample estimator (Liu et al, 2019)}
Intuitively combine sampling with explicit summation.

\bigskip

Goal: approximate $\EE[g(Z)] = \sum_{z \in \ZZ} p(z)g(z) $

Fix  $\ZZ_0 \subset \ZZ$, much smaller in size, so we may sum over it. Yield:

$$\sum_{z \in \ZZ_0} p(z) g(z) + \textcolor{tPeony}{?}$$

\bigskip

\uncover<2->{%
$\textcolor{tPeony}{?} = \sum_{z \in \ZZ \setminus \ZZ_0} p(z)g(z)$
\qquad -- \textcolor{gray}{\emph{not an expectation: let's make it one!}}
}

\bigskip

\uncover<3->{%
Define the \textbf{restricted distribution} over what's left
\begin{equation}
p^{\ZZ \setminus \ZZ_0}(z) \defeq \frac{p(z)}{1 - \sum_{z_0 \in \ZZ_0} p(z_0)}
\end{equation}}
\uncover<4->{%
Then, the remaining term is
\begin{equation}
\textcolor{tPeony}{?} = \sum_{z \in \ZZ \setminus \ZZ_0} p(z)g(z)
= \big(1 - \sum_{z_0 \in \ZZ_0} p(z_0)\big)
\underbrace{\sum_{z \in \ZZ \setminus \ZZ_0} p^{\ZZ \setminus \ZZ_0}(z) g(z)}%
_{\EE_{p^{\ZZ \setminus \ZZ_0}}[g(Z)]}
\end{equation}}
\end{frame}

\begin{frame}{Sum-and-sample estimator (Liu et al, 2019)}
Intuitively combine sampling with explicit summation.
\begin{equation}
\EE_p[g(Z)] =
\underbrace{\sum_{z \in \ZZ_0} p(z)g(z)}_{\text{sum over }\ZZ_0}
+ \big(1 - \sum_{z_0 \in \ZZ_0} p(z_0)\big)
\underbrace{\EE_{p^{\ZZ \setminus \ZZ_0}}[g(Z)]}_{\text{sample from the rest}}
\end{equation}

Properties
\begin{itemize}
\item unbiased (equal sign above)
\item reduces variance (by interpretation as Rao-Blackwellization)
\item good (best) choice of $\ZZ_0$: the $k$ highest-proba configurations
\item when $k \geq |\text{supp}(p)|$, the estimator becomes exact.
\end{itemize}
\end{frame}

%\begin{frame}{Rao-Blackwellization}
%A "general" strategy for improving the variance of an estimator.
%\end{frame}

\begin{frame}{Sampling without replacement (Kool et al, 2020)}
Restricted distribution:
\begin{equation}
p^{\ZZ \setminus \ZZ_0}(z) \defeq \frac{p(z)}{1 - \sum_{z_0 \in \ZZ_0} p(z_0)}
\end{equation}
\uncover<2->{%
Sample with replacement:
\begin{alignat*}{4}
        & z_1 \sim p, &\hspace*{2\arraycolsep}& z_2 \sim p,
&\hspace*{2\arraycolsep}& \dots
&\hspace*{2\arraycolsep}& z_k \sim p \\
\uncover<3->{%
\intertext{Ordered sample without replacement}
     &
b_1 \sim p, &&
b_2 \sim p^{\textcolor{tPeony}{\ZZ \setminus \{b_1\}}},&&
\ldots &&
b_k \sim p^{\textcolor{tPeony}{\ZZ \setminus \{b_1, \dots, b_{k-1}\}}}.}
\end{alignat*}}
\uncover<4->{%
\begin{equation}
p(b_1, \dots, b_k) =
\prod_i p^{\ZZ \setminus \{b_1, \dots, b_{i-1}\}}(b_i) =
\prod_i \frac{p(b_i)}{1 - \sum_{j<i} p(b_j)}
\end{equation}}
\uncover<5->{%
Unordered sample without replacement: discard the order.
\begin{equation}
p(s_1, \dots, s_k) =
\underbrace{\sum_{B^k \in B(S^k)}}_{\text{all possible permutations}}
\prod_i \frac{p(b_i)}{1 - \sum_{j<i} p(b_j)}
\end{equation}}
\end{frame}
\begin{frame}{Single sample estimator}
How can we use a sample $B^k = (b_1, \dots, b_k)$ to estimate $\EE[g(Z)]$?

Idea 1. Recall that
\begin{equation}
p(B^k) =
p(b_1)
~~p^{\ZZ \setminus \{b_1\}}(b_2)
~~p^{\ZZ \setminus \{b_1, b_2\}}(b_3) \ldots
\end{equation}

Sample $B^k$. Ignore everything but $b_1$, yield $g(b_1)$.

\uncover<2->{\textcolor{gray}{(identical to single sample Monte-Carlo)}}

\bigskip

\uncover<3->{\textcolor{tPeony}{Interpretation:} approximate $p(z)$ by
$\delta(b_1)$.}

\end{frame}

\begin{frame}{Stochastic sum-and-sample estimator}
Idea 2. Draw an ordered sample without replacement $B^k$.

Set $\ZZ_0 = \{ b_1, \dots, b_{k-1} \}$.

$b_k$ is by construction a sample from $p^{\ZZ \setminus \ZZ_0}$.
(order matters!)
\begin{equation}
\sum_{j=1}^{k-1} p(b_j)g(b_j)
+ \big(1 - \sum_{i=1}^{k-1} p(b_i)\big)
g(b_k)
%\EE_{p^{\ZZ \setminus \ZZ_0}}[g(Z)]
\end{equation}

\bigskip

Similar to deterministic sum-and-sample.
More general API. \\
(e.g. beam search: can sample $B^k$, cannot exactly get top-k).

\bigskip

\uncover<2->{\textcolor{tPeony}{Interpretation:} approximate $p(z)$ by
\begin{equation*}%
q(b_1) = p(b_1), q(b_2) = p(b_2), ...,
\textcolor{tPeony}{q(b_k) = 1 - \sum_i p(b_i)}, q(b_k+1)=0, ...
\end{equation*}
\textcolor{gray}{(order seems arbitrary!)}}
\end{frame}

\begin{frame}{Unordered set estimator (Kool et al, 2020)}
Sample $B^k$. Discard order, yielding $S^k$. Return

\begin{equation}
    \sum_{s \in S^k} p(s)
~g(s)
~R(S^k, s)
\end{equation}
where $R(S^k, s) \defeq \frac{p^{\ZZ \setminus \{s\}}(S^k \setminus \{s\})}{p(S^k)}$
is the \emph{leave-one-out ratio}.

\bigskip

\uncover<2->{%
Compare to the MC estimator with k samples \textbf{with} replacement:

\begin{equation}
    \frac{1}{k} \sum_{z_1, \dots, z_k \sim p} g(z)
\end{equation}
}


\uncover<3->{\textcolor{tPeony}{Interpretation:}
$\sum_{s \in S^k} p(s) R(S^k, s) = 1$:
~order-unaware reweighting.}

\bigskip

\uncover<4->{
\begin{itemize}
\item Derivation by Rao-Blackwellization of ideas 1 \& 2.

\item $R(S^k, s)$ is nontrivial to compute, can approximate.

\item When $k \geq |\text{supp}(p)|$, the estimator becomes exact.
\end{itemize}}

\end{frame}

\begin{frame}{Results}
Revisit the toy Bernoulli problem with parameter $\theta \in \RR$.

Sample $Z = [Z_1, Z_2, Z_3]: Z_i \sim \text{Bernoulli}(\sigma(\theta))$

$$t = [.60, .51, .48]. \qquad \min_\theta \EE \|Z - t \|^2$$

\centering
\includegraphics[width=.8\textwidth]{toy_results}

\end{frame}

\begin{frame}{Visualization}
{\small Estimators $\EE_p[g] \approx \EE_q[g]$ depicted as the weights $q(z)$.}
\scriptsize

\def\probas{.4, .3, .1, .08, .07, .05}
\def\barw{.45}
\def\barspace{.55}
\def\barh{2}
\tikzset{
lbl/.style={anchor=north,font=\tiny,color=white,outer sep=0,inner sep=1pt},
fillb/.style={fill,gray}}

\begin{columns}[T]
\begin{column}{.33\textwidth}
\begin{tikzpicture}
\foreach \zval [count = \zix] in \probas {
\node[anchor=north] at (0+.5*\barw+\barspace*\zix, 0) {$z_{\zix}$};
\draw[fillb] (0+\barspace*\zix, 0) rectangle (\barw+\barspace*\zix, \barh*\zval);
\ifnum \pgfmathresult > 0
\node[anchor=south,font=\tiny,color=gray] at (0+.5*\barw+\barspace*\zix, \barh*\zval) {\zval};
\fi
}
\end{tikzpicture}

Original p(z)

\bigskip

\begin{tikzpicture}
\foreach \zval [count = \zix] in {0, 1, 0, 0, 0, 0} {
\node[anchor=north] at (0+.5*\barw+\barspace*\zix, 0) {$z_{\zix}$};
\draw[fillb,fill=tPeony] (0+\barspace*\zix, 0) rectangle (\barw+\barspace*\zix, \barh*\zval);
\pgfmathparse{\zval > 0 ? 1 : 0}
\ifnum \pgfmathresult > 0
\node[lbl] at (0+.5*\barw+\barspace*\zix, \barh*\zval) {\zval};
\fi
}
\end{tikzpicture}

Single sample ($z_2$)

\bigskip

\begin{tikzpicture}
\foreach \zval [count = \zix] in {.33, .66, 0, 0, 0, 0} {
\node[anchor=north] at (0+.5*\barw+\barspace*\zix, 0) {$z_{\zix}$};
\draw[fillb,fill=tPeony] (0+\barspace*\zix, 0) rectangle (\barw+\barspace*\zix, \barh*\zval);
\pgfmathparse{\zval > 0 ? 1 : 0}
\ifnum \pgfmathresult > 0
\node[lbl] at (0+.5*\barw+\barspace*\zix, \barh*\zval) {\zval};
\fi
}
\end{tikzpicture}

Sample w/ replacement ($z_2, z_1, z_2$)
\end{column}
\begin{column}{.33\textwidth}

\bigskip

\begin{tikzpicture}
\foreach \zval [count = \zix] in {0, 0, .33, .27, .23, .17}{
\node[anchor=north] at (0+.5*\barw+\barspace*\zix, 0) {$z_{\zix}$};
\draw[fillb] (0+\barspace*\zix, 0) rectangle (\barw+\barspace*\zix, \barh*\zval);
\pgfmathparse{\zval > 0 ? 1 : 0}
\ifnum \pgfmathresult > 0
\node[lbl] at (0+.5*\barw+\barspace*\zix, \barh*\zval) {\zval};
\fi
}
\end{tikzpicture}

Restricted $p^{\ZZ \setminus \{z_1, z_2\}}$

\bigskip

\bigskip


\begin{tikzpicture}
\foreach \zval/\col [count = \zix] in {.4/tBlue, .3/tBlue, 0/tBlue, 0/tBlue, .3/tPeony, 0/tBlue} {
\node[anchor=north] at (0+.5*\barw+\barspace*\zix, 0) {$z_{\zix}$};
\draw[fillb,fill=\col] (0+\barspace*\zix, 0) rectangle (\barw+\barspace*\zix, \barh*\zval);
\pgfmathparse{\zval > 0 ? 1 : 0}
\ifnum \pgfmathresult > 0
\node[lbl] at (0+.5*\barw+\barspace*\zix, \barh*\zval) {\zval};
\fi
}
\end{tikzpicture}

Deterministic sum-and-sample (\textcolor{tBlue}{top-2}, \textcolor{tPeony}{$\{z_5\}$})

\bigskip

\bigskip

\begin{tikzpicture}
\foreach \zval/\col [count = \zix] in {0/tBlue, .3/tBlue, .1/tBlue, 0/tBlue, .6/tPeony, 0/tBlue} {
\node[anchor=north] at (0+.5*\barw+\barspace*\zix, 0) {$z_{\zix}$};
\draw[fillb,fill=\col] (0+\barspace*\zix, 0) rectangle (\barw+\barspace*\zix, \barh*\zval);
\pgfmathparse{\zval > 0 ? 1 : 0}
\ifnum \pgfmathresult > 0
\node[lbl] at (0+.5*\barw+\barspace*\zix, \barh*\zval) {\zval};
\fi
}
\end{tikzpicture}

Stochastic sum-and-sample $(\textcolor{tBlue}{z_3},\textcolor{tBlue}{z_2},\textcolor{tPeony}{z_5})$

\end{column}
\begin{column}{.33\textwidth}
\begin{tikzpicture}
\foreach \zval [count = \zix] in {0, .64, .21, 0, .15, 0}{
\node[anchor=north] at (0+.5*\barw+\barspace*\zix, 0) {$z_{\zix}$};
\draw[fillb] (0+\barspace*\zix, 0) rectangle (\barw+\barspace*\zix, \barh*\zval);
\pgfmathparse{\zval > 0 ? 1 : 0}
\ifnum \pgfmathresult > 0
\node[lbl] at (0+.5*\barw+\barspace*\zix, \barh*\zval) {\zval};
\fi
}
\end{tikzpicture}

Restricted $p^{\{z_2, z_3, z_5\}}$

\bigskip

\bigskip

\begin{tikzpicture}
\foreach \zval [count = \zix] in {0,.429,.294,0,.277,0}{
\node[anchor=north] at (0+.5*\barw+\barspace*\zix, 0) {$z_{\zix}$};
\draw[fillb,fill=tPeony] (0+\barspace*\zix, 0) rectangle (\barw+\barspace*\zix, \barh*\zval);
%\ifnum \numexpr\zval > 0

\pgfmathparse{\zval > 0 ? 1 : 0}
\ifnum \pgfmathresult > 0
\node[lbl] at (0+.5*\barw+\barspace*\zix, \barh*\zval) {\zval};
\fi
}
\end{tikzpicture}

Unordered set
$\{
\textcolor{tPeony}{z_2},
\textcolor{tPeony}{z_3},
\textcolor{tPeony}{z_5}
\}$
\end{column}
\end{columns}
\end{frame}
\end{document}

