
\documentclass[xcolor=table,dvipsnames]{beamer}

\usepackage{mathtools}
\usepackage{amsmath,amssymb,bm}
\usepackage{tikz-dependency}
\usepackage{tikz}
%\usepackage{etex}
\usepackage{framed}
\usepackage[utf8]{inputenc}
\usepackage[default]{lato}
\usepackage{booktabs}
\usepackage[default]{sourcesanspro}
%\usepackage[T1]{fontenc}
\usepackage{xcolor,colortbl}
\usepackage[normalem]{ulem}
\usepackage{nicefrac}

\usepackage{fancybox}
\usepackage{bchart}
\usepackage{ifthen}

\definecolor{mplblue}{HTML}{1F77B4}
\definecolor{mplorange}{HTML}{FF7F03}

\setbeamertemplate{enumerate items}[default]
%\definecolor{tPeony}{RGB}{253,110,138}
\definecolor{tPeony}{RGB}{194,76,99}
\definecolor{tBlue}{RGB}{40,126,145}
\colorlet{myhl}{tBlue!25!white}
\colorlet{mygr}{black!40}
\newcommand{\palert}[1]{\textcolor{tPeony}{#1}}
\beamertemplatenavigationsymbolsempty
\setbeamercolor{alerted text}{fg=tBlue}
%\setbeamercolor{frametitle}{bg=tBlue}
\setbeamercolor{palette primary}{bg=tBlue}
\setbeamercolor{palette secondary}{bg=tBlue!50!black}
\setbeamercolor{palette tertiary}{bg=tBlue!75!black}
\setbeamercolor{palette quaternary}{bg=tBlue}
\setbeamercolor{structure}{fg=tBlue} % itemize, enumerate, etc

\newcommand\EE{\mathbb{E}}
\newcommand\RR{\mathbb{R}}
\newcommand\ZZ{\mathcal{Z}}
\newcommand\defeq{\coloneqq}
\newcommand\DP[2]{\langle #1, #2 \rangle}
\def\newblock{}

\title{Learning with Differentiable Perturbed Optimizers}
\author{presentation by Vlad Niculae}

\begin{document}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
Based on the following papers.

\begin{itemize}
\item
\textbf{Learning with Differnetiable Perturbed Optimizers}
Berthet, Blondel, Teboul, Cuturi, Vert, Bach
\end{itemize}

\emph{Thanks to Mathieu Blondel for discussion!}

\end{frame}

\begin{frame}
Given a set of points $\mathcal{Y}$,
\uncover<2->{and their convex hull $\mathcal{C}$.}
\uncover<3->{%
\begin{equation*}
F(\theta) = \max_{y \in \mathcal{C}} \DP{y}{\theta}
\qquad
y^\star(\theta) = \arg\max_{y \in \mathcal{C}} \DP{y}{\theta}
\end{equation*}}

\onslide<4->{%
\alert{For differentiability:} Smoothed argmax $\DP{y}{\theta} - \Omega(y)$
with strongly convex $\Omega$.
(e.g.\ marginal inference.) \alert{But it may be hard to compute.}}

\centering

\begin{tikzpicture}
\node[
    ultra thick,
    fill opacity=.15,
    minimum size=2.5cm,
    regular polygon, regular polygon sides=6] (mp) {};
%\node[label=east:{\small$\mathcal{C}$}] at (mp.corner 5) {};
\foreach \i in {1, ..., 6}%
{
    \draw[mplblue,fill] (mp.corner \i) circle[radius=3pt];
}

\node<2->[
    ultra thick,
    draw=mplblue,
    fill=mplblue,
    fill opacity=.15,
    minimum size=2.5cm,
    regular polygon, regular polygon sides=6] {};

\onslide<3>{
\path[draw, very thick, color=mplorange!40!black] (-2, 0) -- (2, -2);
\path[draw, very thick, color=mplorange!50!black] (-2, 0.5) -- (2, -1.5);
\path[draw, very thick, color=mplorange!60!black] (-2, 1) -- (2, -1);
\path[draw, very thick, color=mplorange!70!black] (-2, 1.5) -- (2, -0.5);
\path[draw, very thick, color=mplorange!80!black] (-2, 2) -- (2, 0);
\draw[color=mplorange,fill] (mp.corner 1) circle[radius=4pt];
}

\onslide<4->{
\node at ($(mp.corner 2)!.5!(mp.corner 6)$) (smoothed) {};
\draw[color=mplorange,fill] (smoothed) circle[radius=4pt];
\draw[color=mplorange!80!black] (smoothed) circle[radius=10pt];
\draw[color=mplorange!60!black] (smoothed) circle[radius=20pt];
\draw[color=mplorange!40!black] (smoothed) circle[radius=30pt];
}
\end{tikzpicture}

\end{frame}

\begin{frame}
Smoothing via perturbation
\begin{equation*}
F_\varepsilon(\theta) =
\uncover<2->{\mathbb{E}_Z[}%
\max_{y \in \mathcal{C}} \DP{y}{\theta + \varepsilon Z}
\uncover<2->{]}
\qquad
y_\varepsilon^\star(\theta) =
\uncover<2->{\mathbb{E}_Z[}%
\arg\max_{y \in \mathcal{C}} \DP{y}{\theta + \varepsilon Z}
\uncover<2->{]}
\end{equation*}

\begin{columns}[T]
\begin{column}{.5\textwidth}

\uncover<3->{Corresponds to $\Omega$ smoothing for some $\Omega$.}

\bigskip

\uncover<4->{%
Induced $\Omega_\varepsilon$ is strongly-convex, Legendre-type.
Resulting $F_\varepsilon$ is twice differentiable, Lipschitz.}

\bigskip

\uncover<5->{%
We don't need an explicit expression for $\Omega_\varepsilon$;
we may evaluate $y^\star_\varepsilon$ via Monte Carlo.
\alert{Sufficient for learning with the $L_{\Omega_\varepsilon}$ FY loss.}}
\end{column}
\begin{column}{.5\textwidth}
\includegraphics[width=\textwidth]{perturb_fig1.png}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\begin{equation*}
F_\varepsilon(\theta) =
\uncover<2->{\mathbb{E}_Z[}%
\max_{y \in \mathcal{C}} \DP{y}{\theta + \varepsilon Z}
\uncover<2->{]}
\qquad
y_\varepsilon^\star(\theta) =
\uncover<2->{\mathbb{E}_Z[}%
\arg\max_{y \in \mathcal{C}} \DP{y}{\theta + \varepsilon Z}
\uncover<2->{]}
\end{equation*}

\bigskip

$Z: \quad \mathrm{d} \mu(z) \propto \exp(-\nu(z)) \mathrm{d}z$.
{\small \textcolor{gray}{(e.g.\ std.\ normal: $\nu(z) = .5 \|z\|^2_2$)}}

\bigskip

{\footnotesize
\begin{equation*}
\begin{align}
\mu(A) &= \int_A f \mathrm{d}z\qquad&&\text{\color{gray}(measure of any set A)} \\
\frac{\mathrm{d} \mu}{\mathrm{d}z} &= f\qquad&&\text{\color{gray}(Radon-Nykodim
derivative = the p.d.f.\ of Z)} \\
\end{align}
\end{equation*}
}

\begin{framed}
\alert{Main (imo) result}
\begin{align}
\frac{\partial y^\star_\epsilon}{\partial \theta} (\theta) &=
\mathbb{E}\left[\frac{1}{\varepsilon} y^\star(\theta + \varepsilon Z)
\nabla \nu(Z)^\top \right] \\
& =\mathbb{E}\left[\frac{1}{\varepsilon^2}F(\theta + \varepsilon Z)
~\Big(\nabla \nu(Z) \nabla \nu(Z)^\top\!
- \nabla^2 \nu(Z)\Big)\right]
\end{align}
\end{framed}

\end{frame}

\begin{frame}
Example: $\mathcal{C} = \triangle,\quad \varepsilon=1, \quad \nabla \nu(z) = z$ (standard normal).

\begin{align}
\scriptsize \mathbb{E}\left[\frac{1}{\varepsilon} y^\star(\theta + \varepsilon Z)
\nabla \nu^\top \right]\!\!\!&=\!
\mathbb{E}[\arg\max(\theta + Z) Z^\top]  \\
\scriptsize \mathbb{E}\left[\frac{1}{\varepsilon^2}F(\theta + \varepsilon Z)
\left(\nabla \nu \nabla \nu^\top\!\!- \nabla^2 \nu\right)\right]
\!\!\!&=\!
\mathbb{E}[\max(\theta + Z) \left(ZZ^\top\!\!- I\right)]
\end{align}

\begin{itemize}
\item (4) doesn't depend on the argmax, just on its score!
\\ {\color{gray}(are there cases where max is much faster than argmax?)}
\item (3) must be symmetric in the limit, but each term won't be.
\\ {\color{gray}(symmetrizing could improve accuracy)}
\item Small-sample approx of (3) is sparse, but (4) is dense.
\end{itemize}

\end{frame}

\begin{frame}
\centering%
MC gradients vs.\ finite difference approx:

\includegraphics[width=\textwidth]{perturb_plot.pdf}
\end{frame}


\end{document}

